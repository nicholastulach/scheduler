# Scheduler
## Description
A small script to create a multi-track single-day conference from a text file containing conference session information. 
## Requirements
* Python 3.X
* Conference session data in text format: one title per line, with either “lightning” or “Xmin” appended to the end of each line.
* [Poetry](https://python-poetry.org) (optional)
## Setup
1. [Clone this repo](https://gitlab.com/nicholastulach/scheduler.git) to your machine
2. From terminal, run `python scheduler.py --filename input.txt`
3. View output in stdout