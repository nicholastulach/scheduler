import argparse
import math
import os
import re

from datetime import date, datetime, time, timedelta


class ConferenceSession:
    """An object representing a conference session with attributes for its
    length (in minutes), title, and original line
    """

    def __init__(self, line):
        if "lightning" in line:
            self.length = 5
            self.title = line.replace(" lightning\n", "")
            self.ogline = line
        # NOTE: Assumes well-formed data. Could check for poorly formed data
        # here and either skip or raise exception.
        else:
            (title, length, _) = re.split(r"(\d+)", line)
            self.length = int(length)
            self.title = title[:-1]
            self.ogline = line


class FullTrackError(Exception):
    """Raised if a track object is full of sessions"""

    pass


class Track:
    """An object representation of a conference track with a 3-hour morning
    session and a 4-hour afternoon session, with a one hour lunch break at noon
    and a networking event between 4pm and 5pm
    """

    def __init__(self, name):
        self.name = name
        self.morn_max = 3 * 60
        self.aft_max = 4 * 60
        self.morn = []
        self.aft = []

    def add(self, sesh):
        if (sesh.length + sum(s.length for s in self.morn)) <= self.morn_max:
            self.morn.append(sesh)
        elif (sesh.length + sum(s.length for s in self.aft)) <= self.aft_max:
            self.aft.append(sesh)
        else:
            raise FullTrackError

    def __str__(self):
        time_format = "%I:%M%p"
        cur_time = datetime.combine(date.today(), time(9, 0))
        min_network_time = datetime.combine(date.today(), time(16, 0))
        morn_text_list = []
        aft_text_list = []
        for sesh in self.morn:
            morn_text_list.append(f"{cur_time.strftime(time_format)} {sesh.ogline}")
            cur_time = cur_time + timedelta(minutes=sesh.length)
        morn_text = "\n".join(morn_text_list)

        cur_time = datetime.combine(date.today(), time(13, 0))
        for sesh in self.aft:
            aft_text_list.append(f"{cur_time.strftime(time_format)} {sesh.ogline}")
            cur_time = cur_time + timedelta(minutes=sesh.length)
        aft_text = "\n".join(aft_text_list)

        lunch_time = datetime.combine(date.today(), time(12, 0))
        network_time = max(min_network_time, cur_time)
        return (
            f"Track {self.name}\n"
            f"{morn_text}"
            f"\n{lunch_time.strftime(time_format)} Lunch\n\n"
            f"{aft_text}"
            f"\n{network_time.strftime(time_format)} Networking Event"
        )


def schedule(infile):
    """Uses input file (infile) to schedule conference tracks automatically.
    Relies on clean data and does not fail gracefully.
    """
    with open(infile, "r") as fp:
        sessions = [ConferenceSession(line) for line in fp]
        total_session_time = sum(sesh.length for sesh in sessions)

        # "tracks_needed" assumes that all the sessions are relatively evenly
        # distributed in length. If there are a lot of really long sessions,
        # this will break by not accommodating all the sessions.
        tracks_needed = math.ceil(total_session_time / (60 * 7))

        for i in range(1, tracks_needed + 1):
            track = Track(i)
            while True:
                try:
                    track.add(sessions.pop(0))
                except (FullTrackError, IndexError):
                    print(track)
                    print("= " * 40)
                    break


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--filename",
        type=str,
        required=True,
        help="POFD data file",
    )
    args = parser.parse_args()
    schedule(args.filename)
